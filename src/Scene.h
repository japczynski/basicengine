#pragma once

#include "common.h"
#include <string>
#include <vector>
#include <memory>
#include "Model.h"
#include "Camera.h"
#include "Node.h"

struct Scene {
    std::vector<std::shared_ptr<Model>> models;
    std::shared_ptr<Camera> camera;
    std::shared_ptr<Node> sceneRoot;

    Scene();
    ~Scene();
    void Update();
    void Render();
};