#pragma once

#include <vector>
#include <memory>

struct Node {
    std::vector<std::shared_ptr<Node>> children;
    std::shared_ptr<Node> parent;
    std::string name;

    Node(const std::string nodeName);
    ~Node();
    //void AddChild(std::shared_ptr<Node> newChild);
};