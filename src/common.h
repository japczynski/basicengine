#pragma once

#include <Windows.h>

#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/GL.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>