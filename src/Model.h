#pragma once

#include "common.h"
#include "Camera.h"
#include "Node.h"
#include <string>

struct Model : Node {
    unsigned int vertexCount;
    GLuint vertexBuffer;
    GLuint uvBuffer;
    GLuint textureID;
    GLuint programID;
    glm::mat4 matrix;

    Model(const std::string modelName);
    ~Model();
    void Destroy();
    void Load(const unsigned int newVertexCount,
              const GLfloat 	 vertexes[],
              const GLfloat 	 uvs[],
              const std::string	 texture,
              const std::string	 vertexShader,
              const std::string	 fragmentShader);
    void Render(const std::shared_ptr<Camera> camera);
};

