#include "Scene.h"

Scene::Scene() {
    camera = std::make_shared<Camera> ();
    sceneRoot = std::make_shared<Node> ("Root");
}

Scene::~Scene() {
    models.clear();
}

void Scene::Update() {
    camera->Update();
}

void Scene::Render() {
    for (int i = 0; i < models.size(); i++) {
        auto currentModel = models[0];
        currentModel->Render(camera);
    }

    // Swap buffers
    glfwSwapBuffers();
}