#pragma once

#include "common.h"

struct Controllable {
    glm::mat4 Matrix;
};