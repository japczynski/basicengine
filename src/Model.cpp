#include "Model.h"

#include "common.h"
#include <string>
#include "tut_common/shader.hpp"
#include <iostream>

Model::Model(const std::string modelName): Node(modelName) {
    matrix = glm::mat4(1.0);
    vertexBuffer = 0;
    uvBuffer = 0;
    textureID = 0;
    programID = 0;
    vertexCount = 0;

    std::cout << "Model " << name << " created" << std::endl;
}

Model::~Model() {
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteBuffers(1, &uvBuffer);
    glDeleteProgram(programID);
    glDeleteTextures(1, &textureID);

    std::cout << "Model " << name << " destroyed" << std::endl;
}

void Model::Destroy() {
}

GLuint loadTGA_glfw(std::string imagepath) {
    // Create one OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);

    // Bind the newly created texture
    // all future texture operations will modify this texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    // Read the file, call glTexImage2D with the right parameters
    glfwLoadTexture2D(imagepath.c_str(), 0);

    // Nice trilinear filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Return the ID of the texture we just created
    return textureID;
}

void Model::Load(const unsigned int newVertexCount,
                 const GLfloat  	vertexes[],
                 const GLfloat	    uvs[],
                 const std::string	texture,
                 const std::string	vertexShader,
                 const std::string	fragmentShader) {
    
    vertexCount = newVertexCount;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertexCount*3*sizeof(GLfloat), vertexes, GL_STATIC_DRAW);

    glGenBuffers(1, &uvBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertexCount*2*sizeof(GLfloat), uvs, GL_STATIC_DRAW);

    textureID = loadTGA_glfw(texture.c_str());
    programID = LoadShaders(vertexShader.c_str(), fragmentShader.c_str());
}

void Model::Render(const std::shared_ptr<Camera> camera) {
        glUseProgram(programID);

        GLuint MatrixID = glGetUniformLocation(programID, "MVP");
        GLuint TextureID = glGetUniformLocation(programID, "myTextureSampler");
            
        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureID);
        // Set our "myTextureSampler" sampler to user Texture Unit 0
        glUniform1i(TextureID, 0);

        // Set up buffer 0 (vertex location buffer)
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        // Set up buffer 1 (vertex uv buffer)
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

        // Calculate ModelViewProjection matrix and pass it to shaders
        glm::mat4 MVP = camera->ProjectionMatrix * camera->Matrix * matrix;
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

        glDrawArrays(GL_TRIANGLES, 0, vertexCount);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
}