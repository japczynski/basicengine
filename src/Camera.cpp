#include "Camera.h"

#include <iostream>

Camera::Camera() {
    aspect = 4.0f/3.0f;
    fov = 45.0f;

        //camera->Matrix = glm::lookAt(
    //	glm::vec3(4, 3, -3),   // camera position
    //	glm::vec3(0, 0, 0),   // camera interest
    //	glm::vec3(0, 1, 0)    // head up vector
    //);

    position = glm::vec3(4, 3, -3);
    Matrix = glm::lookAt(position,
                         glm::vec3(0.0f, 0.0f, 0.0f),
                         glm::vec3(0.0f, 1.0f, 0.0f));

    ProjectionMatrix = glm::perspective(
        fov, aspect, 0.1f, 100.0f);

    std::cout << "Camera created" << std::endl;
}

Camera::~Camera() {
    std::cout << "Camera deleted..." << std::endl;
}

void Camera::ComputeProjection() {
    ProjectionMatrix = glm::perspective(
        fov, aspect, 0.1f, 100.0f);
}

void Camera::SetAspect(const float newAspect) {
    aspect = newAspect;
    ComputeProjection();
}

void Camera::SetFov(const float newFov) {
    fov = newFov;
    ComputeProjection();
}

void Camera::Update() {
}