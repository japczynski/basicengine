#include "Controller.h"

#include "common.h"
#include <string>
#include <vector>

Controller::Controller() {
    glfwDisable(GLFW_MOUSE_CURSOR);
    lastTime = glfwGetTime();
    speed = 3.0f;
    mouseSpeed = 0.05f;
    verticalAngle = 0.0f;
    horizontalAngle = 3.14f;
    position = glm::vec3(0, 0, 5);
}

void Controller::Update() {
    glfwGetMousePos(&xpos, &ypos);
    glfwGetWindowSize(&width, &height);
    glfwSetMousePos(width/2, height/2);

    double currentTime = glfwGetTime();
    float deltaTime = float(currentTime - lastTime);
    lastTime = currentTime;

    horizontalAngle += mouseSpeed * deltaTime * float(width/2 - xpos);
    verticalAngle += mouseSpeed * deltaTime * float(height/2 - ypos);

    // Direction vector
    glm::vec3 direction(
        cos(verticalAngle) * sin(horizontalAngle),
        sin(verticalAngle),
        cos(verticalAngle) * cos(horizontalAngle));

    // Right vector
    glm::vec3 right = glm::vec3(
        sin(horizontalAngle - 3.14f/2.0f),
        0,
        cos(horizontalAngle - 3.14f/2.0f));

    // Up vector : perpendicular to both direction and right
    glm::vec3 up = glm::cross(right, direction);

    // Move forward
    if (glfwGetKey('W') == GLFW_PRESS) {
        position += direction * deltaTime * speed;
    }
    if (glfwGetKey('A') == GLFW_PRESS) {
        position -= right * deltaTime * speed;
    }
    if (glfwGetKey('S') == GLFW_PRESS) {
        position -= direction * deltaTime * speed;
    }
    if (glfwGetKey('D') == GLFW_PRESS) {
        position += right * deltaTime * speed;
    }

    controllable->Matrix = glm::lookAt(
        position,
        position + direction,
        up);
}