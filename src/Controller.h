#pragma once

#include "common.h"
#include "Controllable.h"
#include <memory>

struct Controller {
    int xpos;
    int ypos;
    int width;
    int height;
    float verticalAngle;
    float horizontalAngle;
    glm::vec3 direction;
    glm::vec3 position;

    float mouseSpeed;
    float speed;

    double lastTime;
    std::shared_ptr<Controllable> controllable;

    Controller();
    void Update();
};