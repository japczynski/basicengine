#pragma once

#include "common.h"
#include "Controllable.h"
#include <vector>

struct Camera: Controllable {
    //glm::mat4 Matrix;
    glm::mat4 ProjectionMatrix;
    float aspect;
    float fov;
    glm::vec3 position;

    Camera();
    ~Camera();
    void ComputeProjection();
    void SetAspect(const float newAspect);
    void SetFov(const float newFov);
    void Update();
};